import React from 'react';

export const Loading = () => {
    return(
        <div className="col12">
            <span className="fa fa-spinner fa-pulse fa-3x fa-fx text-primary"></span>
            <p>Loading...</p>
        </div>
    )
}